---
MorpheusModelID: M5492

contributors: [G. Burger, L. Brusch]

title: Initialize cell population from CSV
date: "2023-11-06T10:42:00+02:00"
lastmod: "2023-11-07T14:49:00+01:00"

tags:
- 2D
- Adhesion
- Cellular Potts Model
- Cell Sorting
- CPM
- CSVReader
- Initial Condition
- Multicellular Model
- Spatial Model
- Stochastic Model

---

{{% callout note %}}
This model also requires the separate file [`Population_M.csv`](#downloads) with the list of lattice nodes where cells shall be initialized.
{{% /callout %}}

## Introduction

Initializing the spatial configuration of cells is a key step of every simulation and Morpheus offers all the flexibility for this:
1. the model section ```CellPopulations/Population``` (in the GUI and xml) can declare geometrical rules incl.
- ```InitCellObjects/Arrangement``` (cell repetitions in (x,y,z), displacements in (x,y,z) and random_displacement) of different cell shapes with adjustable sizes and rotation angles (Point, Box, Sphere, Ellipsoid, Cylinder)
- ```InitCircle``` (seed cells in a circular subdomain), ```InitDistribute```, ```InitHexLattice```, ```InitPoissonDisc```, ```InitRectangle```, ```InitVoronoi```
2. you can overlay multiple ```CellPopulations/Population``` groups with the above constructs
3. all these arrangements together can be masked by ```Lattice/Domain```
4. you may specify the initial cell shapes or just set single points that will inflate into cell shapes during an initial transient of the model simulation
5. you can load a ```CellPopulations/Population``` from files, both CSV (for lists of nodes for seeding as shown below) or TIFF (for pixel-based spatial layout where pixel color is interpreted as ```CellID```, so you can segment from microscopy images or draw arbitrary areas each with the same color)
6. you can run a helper model that distributes the cells as you need and saves a snapshot of the spatial configuration with ```Time/SaveInterval``` (then load the snapshot ```model.xml``` and copy/paste its ```CellPopulations/Population``` from the snapshot model to your main model)

All these rules for initializing the spatial configuration of cells are included in the same xml file as the model rules. So you can reproduce the exact same initialization from the model.xml.

For a realistic epithelial cell arrangement, see this [Blog post](https://morpheus.gitlab.io/post/2019/07/01/poisson-disc-sampling/).

## Description

Here we use the file ```Population_M.csv``` with a list of seeding positions to initialize one of two cell populations with the shape of the letter M. 

![](plot_00000.png "Snapshot at time=0 with the loaded single nodes forming an M for the (red) cell population superimposed on another randomly placed set of nodes for the (yellow) cell population.")

![](plot_00100.png "Snapshot at time=100, color code as described for previous figure.")

![](movie.mp4)
The video shows how the single nodes inflate to cell shapes during 100 Monte Carlo steps.
