---
MorpheusModelID: M2013

title: Actin Waves

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- 1D
- 2D
- Actin
- Actin Wave
- F-actin
- Filament
- Lattice
- Linear Lattice
- Microfilament
- Partial Differential Equation
- PDE
- Reaction Rate
- Square Lattice
- Wave
- Well-mixed

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Spatial relaxation oscillator produces spatial dynamics and spiral waves

## Introduction

We consider a full spatial variant of the <a href="/model/m2012/" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M2012</code>">model</a> of a F-actin negative feedback to GTPase system.

## Description

To do so, we add diffusion terms to well-mixed GTPase equations. The PDEs are:

$$\begin{align}
\frac{\partial G}{\partial t} &= D\frac{\partial^2 G}{\partial x^2} + (b+\gamma \frac{G^n}{1+G^n})G_{\mathrm i} - G(\eta + sF) \\\\
\frac{\partial G_{\mathrm i}}{\partial t} &= D_{\mathrm i}\frac{\partial^2 G_{\mathrm i}}{\partial x^2} - (b+\gamma \frac{G^n}{1+G^n})G_{\mathrm i} + G(\eta + sF) \\\\
\frac{\partial F}{\partial t} &= \epsilon(k_{\mathrm n}G-k_{\mathrm s}F) \\\\
\end{align}$$

We explore the equation system first in a single spatial dimension in {{< model_quick_access "media/model/m2013/ActinWavesPDE_main.xml" >}} and continue in two spatial dimensions using a <span title="//Space/Lattice[@class='square']">`square`</span> lattice in place of the <span title="//Space/Lattice[@class='linear']">`linear`</span> lattice in {{< model_quick_access "media/model/m2013/ActinWavesPDE_2D.xml" >}}, with the actin reaction rate <span title="//Global/System/Constant[@symbol='epsilon']">$`\epsilon`$</span> changed from $`0.001`$ to $`0.01`$ to speed up the dynamics.

## Results

Consider the actin waves PDE system in 1D spatial coordinates (top figures) and in 2D (lower panels):

![](MorpheusActinWaves1Dhor.png "Simulation of the actin waves PDE system in 1D, showing <span title='//Global/Field[@symbol=&#39;a&#39;]'>$`G(x, t)`$</span> in the top row and <span title='//Global/Field[@symbol=&#39;F_a&#39;]'>$`F(x, t)`$</span> in the bottom row for several values of the feedback parameter <span title='//Global/System/Constant[@symbol=&#39;s&#39;]'>$`s`$</span>. Produced with [`ActinWavesPDE_main.xml`](#model).")

<figure>
  ![](M2013_actin-waves_movie_ActinWavesPDE_main.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2013/ActinWavesPDE_main.xml" >}} with the profile of all three variables active GTPase <span title="//Global/Field[@symbol='a']">$`G(x, t)`$</span>, inactive GTPase <span title="//Global/Field[@symbol='i']">$`G_{\mathrm i}(x, t)`$</span> and F-actin <span title="//Global/Field[@symbol='F_a']">$`F(x, t)`$</span> changing over time.
  </figcaption>
</figure>

![](MorpheusActinWaves2D.png "Simulation of the actin waves PDE system in 2D, showing (right, left) <span title='//Global/Field[@symbol=&#39;a&#39;]'>$`G(x,y,t)`$</span> and <span title='//Global/Field[@symbol=&#39;F_a&#39;]'>$`F(x,y,t)`$</span> (side by side) over time. Produced with [`ActinWavesPDE_2D.xml`](#downloads).")

<figure>
![](M2013_actin-waves_movie_ActinWavesPDE_2D.mp4)
<figcaption>Simulation video of {{< model_quick_access "media/model/m2013/ActinWavesPDE_2D.xml" >}}</figcaption>
</figure>
