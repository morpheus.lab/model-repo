---
MorpheusModelID: M2005

title: "FitzHugh–Nagumo ODEs"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- Bonhoeffer–Van der Pol Oscillator
- Excitable System
- Excitation
- FHN
- FitzHugh–Nagumo
- FitzHugh–Nagumo Model
- FN
- Neuron
- Neuronal Excitation
- Neuronal Firing
- ODE
- Ordinary Differential Equation
- Oscillator
- Relaxation Oscillator
- Well-mixed
- Well-mixed Model
- Well-mixed System

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> ODEs for the FitzHugh–Nagumo well-mixed system

## Introduction

The FitzHugh–Nagumo (FN) model is commonly used as a toy model for neuronal excitation.
## Description

Everything is well-mixed and two ODEs are solved for two variables, <span title="//Global/Variable[@symbol='u']">$`u`$</span> (voltage) and <span title="//Global/Variable[@symbol='v']">$`v`$</span> (recovery variable).

## Results

The figure below shows behavior of the FitzHugh–Nagumo ODEs, the variables <span title="//Global/Variable[@symbol='u']">$`u`$</span>, <span title="//Global/Variable[@symbol='v']">$`v`$</span> are plotted against time.

![](FN.png "Variables <span title='//Global/Variable[@symbol=&#39;u&#39;]'>$`u`$</span>, <span title='//Global/Variable[@symbol=&#39;v&#39;]'>$`v`$</span> plotted against time.")