---
MorpheusModelID: M6696

title: "A Multi-cell, Multi-scale Model of Vertebrate Segmentation and Somite Formation"

authors: [S. D. Hester, J. M. Belmonte, J. S. Gens, S. G. Clendenon, J. A. Glazier]
contributors: [J. Buerger, L. Brusch]
submitters: [J. Buerger, L. Brusch]
curators: [D. Jahn]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "10.1371/journal.pcbi.1002155"
  title: "A Multi-cell, Multi-scale Model of Vertebrate Segmentation and Somite Formation"
  journal: "PLoS Comput. Biol."
  volume: 7
  issue: 10
  page: e1002155
  year: 2011
  original_model: false
  preprint: false

tags:
- 2D
- Adhesion
- Agent-based Simulation
- Cell Division
- Cellular Potts Model
- Chick
- CompuCell3D
- CPM
- Delta-Notch
- FGF
- Multicellular Model
- Partial Differential Equation
- PDE
- Segmentation clock
- Signaling
- Somitogenesis
- Spatial Model
- Stochastic Model
- Wnt

#categories:
#- DOI:10.1371/journal.pcbi.1002155
---
> How can a densely packed PSM tissue mechanically split up into a string of separate somites?

## Introduction

Somitogenesis is the defining developmental process of vertebrate species and essential for the segmentation of the main body axis and, among other repetitive structures, the formation of vertebrae. Early in vertebrate embryo development, individual segments known as somites sequentially bud off the presomitic mesoderm (PSM), pairwise on either side of the notochord. This periodic process is controled by the phases of a molecular oscillator, termed the segmentation clock, in the PSM cells. Clock phases affect gene expression, including adhesion proteins of the Eph and ephrin families, and differentiation of cellular subpopulations within each presumptive somite. An important question, addressed by the presented model, is how the densely packed PSM tissue mechanically splits up into a string of separate somites on each side of the body axis.

![Chick Embryo](chick_embryo.png "Chick embryo at 36h of incubation with somites emerging from the PSM (both pink). The simulation domain on one side of the symmetric embryo is outlined. Credit: Neural tube and somites in chick embryo by Prof. R. Bellairs. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Wellcome Collection**](https://wellcomecollection.org/works/zghsz3eh))")

### The Clock-Wavefront Model

To read out the phase of the segmentation clock at a specific moment, Cooke and Zeeman in 1976 had proposed the clock-wavefront model, _i.e._ a threshold value of a morphogen gradient to impose the moment of clock read-out. The morphogene gradient would move posteriorly with PSM outgrowth such that the threshold value gives a wavefront. 

The presented model includes a field of Fibroblast growth factor 8 (FGF8) as the morphogen. Produced and secreted by the PSM cells, extracellular FGF8 diffuses freely and is degraded. Cells sense their local FGF8 concentration and compare it to a specific threshold value. When both match then the differentiation process initiates, contingent on the state (molecular concentrations representing the phase) of the intracellular segmentation clock.

The segmentation clock is modeled as a system of ODEs for the concentrations of signaling molecules in each PSM cell. Delta-Notch signaling couples any PSM cell to the states of all its neighbors. Though this clock model comprises 20 ODEs and 52 parameters, only three variables are crucial for read-out: lunatic fringe protein (Lfng) concentration, Axin2 concentration, and Beta-Catenin concentration. Each variable dominates one of three clock phases. These three concentration values at the wavefront determine the differentiation process. Consequently, somites in this model become composed of three distinct cell layers: top, core, and bottom.

Details and parameter values of the presented model had been adjusted in the cited publication for a chick embryo.

## Model Description

The cited publication thoroughly documents the model which was developed with the software [CompuCell3D](https://compucell3d.org) and the model files were shared with the publication, see [Text S2](https://doi.org/10.1371/journal.pcbi.1002155.s014).
The model replication in Morpheus, as presented here, exactly follows the paper's detailed model description. 
This is possible for CPM-based models as both simulators provide solvers for the CPM.
The technical settings of node neighborhoods on the lattice for surface estimation and CPM node updates are identical between CompuCell3D and Morpheus when the latter is run in surface scaling mode `classic`, _i.e._ surface length with the fixed 1st order neighborhood kernel but contact energies with the chosen neighborhood kernel, here 2nd order.

In the following, key aspects of the original model (and likewise of its replication) are summarized.

The model considers a 2D cross section of the embryo and just one half of the left-right symmetric process, hence a single string of somites. The 2D square lattice is spanned by the anterior-posterior body axis (top-down in following images) and the medio-lateral axis (left-right).

Length and time units are set as follows: a lattice node has a length of 1.43 µm and a model time step is 1 MCS and corresponds to 0.015 min. As one somite in chick is formed every 90 min, this occurs every 6000 time steps in the model.

### Initial Conditions

The model starts with two columns of 150 immobile wall cells each, maintaining the PSM's shape. The domain's top corresponds to anterior and bottom to posterior. At the top, we initialize three layers of `PSM cells`, with a layer of `Source cells` underneath (these cell type names as literally used in the computational model are distinguished by code highlighting).

![Initial Cell Configuration](initial_condition.png "<strong>Figure 1.</strong> Initial cell configuration in the Morpheus model, matching the original model.")

### PSM Growth

In this model, only the (artificial layer of) `Source cells` divide such that normal (linear) PSM growth results. Each `Source cell` grows (target volume and target surface are gradually increased and CPM updates let the cell shape follow these targets) at a rate set by the parameter `gR=0.120` in model section `Global`. Once a `Source cell` has exceeded twice its initial cell volume, it divides. Daughter cells that stay at the tissue edge where they keep contact with the medium stay `Source cells` while the other daugther cell when it gets surrounded by other cells will become a `PSM cell`. This division model, combined with a ConnectivityConstraint for unfragmented cell shapes, ensures a consistent layer of `Source cells` and supplies new `PSM cells` at a constant rate. Thus, the PSM grows towards the posterior end while individual cells move relatively little on random walks.

### FGF8 Field

The FGF8 concentration is modeled via a reaction-diffusion PDE: $$ \frac{\partial FGF8}{\partial t} = D \nabla^2 FGF8 - k \cdot FGF8 + S(x,t) $$

with uniform and isotropic diffusion and degradation rate `k`. The source term $ S(x,t) $ at any time point depends on the areas currently occupied by individual cells and on the specific FGF8 secreation fluxes by individual cells. Each `PSM cell` and `Source cell` produces FGF8 and secretes it. This is modeled by assigning a fixed amount of mRNA (that encodes for FGF8) to `Source cells` and by computing for each `PSM cell` a concentration of mRNA. Abstracting many biological details, the FGF8 secreted by a cell is set proportional to its current mRNA amount. Intracellular mRNA decays exponentially (except in `Source cells`) at a considerably smaller rate than the extracellular FGF8 decays.
Combined with PSM growth, this leads to the formation of a spatial FGF8 gradient that moves posterior as the PSM grows.

<figure id="vid-1">
  <div style="display: flex; justify-content: space-between;">
    <div style="flex: 1; padding-right: 5px;">
      ![PSM growth submodel](PSM_submodel.mp4)
    </div>
    <div style="flex: 1; padding-left: 5px;">
      ![Traveling FGF8 wavefront](fgf_grad.mp4)
    </div>
  </div>
  <figcaption style="margin-top: -1.75em;">
    <strong>Videos of the Morpheus simulations.</strong> <strong>Left:</strong> Submodel for PSM growth, matching the original model. The tissue elongates at a constant speed that is proportional to the volume growth parameter of <code>Source cells</code>. <strong>Right:</strong> Submodel for PSM growth and FGF8 dynamics. To obtain an FGF8 gradient that spans the entire anterior-posterior axis, FGF8 secretion has to remain active in determined cells.
  </figcaption>
</figure>

![Comparison](mfgf_figure.png "<strong>Figure 2.</strong> Comparison of FGF8 mRNA profiles at equidistant time points along the middle axis. Morpheus results are superimposed onto the published [Figure 6C](https://doi.org/10.1371/journal.pcbi.1002155.s002). Values from the original publication are displayed in full lines, the ones from Morpheus with red dots.")

### Clock-dependend Determination and Differentiation

As described in the Introduction, the differentiation is triggered by reaching a certain FGF8 threshold and specified by the state of the segmentation clock. The implementation for the clock as a coupled system of ODEs is straight forward, both in CompuCell3D and Morpheus.
The sensing of extracellular FGF8 concentration occurs at a single lattice node at the cell's center of mass, both in CompuCell3D and Morpheus.

The differentiation process is modeled as follows: When a cell hits the FGF8 determination threshold, it transitions to a "determined cell," an intermediary state between PSM cells and the fully differentiated cells. Once determined, a cell's final type among the three somitic cell layers is set with a delay. This intermediate step is necessary for cell-sorting, hence, already differentiated cells and determined cells differ in their contact energies.

Here, time courses of the three clock outputs from an isolated cell with fixed FGF8 and Wnt3 input values are compared to the published results.

![Figure4Rep](clock.png "<strong>Figure 3.</strong> Results by the model in Morpheus which reproduce the published results below. Specification of fewer digits in the initial condition may cause slight deviations at the start.")

![Figure4](fig4C.png "<strong>Figure 4.</strong> This is [Figure 4C](https://doi.org/10.1371/journal.pcbi.1002155.g004) of the cited publication. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Hester _et al._**](#reference))")

### Cell-Cell Interactions and Somite Formation

The model comprises 8 biologically motivated cell types: `Source`, `PSM`, `preEphA4`, `preEphrinB2`, `preCore`, `EphA4`, `EphrinB2`, `Core`.
Contact energies are defined for each pair of cell types.

## Results

As in the cited publication, the modularity of the model allows to analyze and compare results of its separated submodels (see figures and videos above), now followed by the complete model dynamics.

Here, all submodels are coupled by feedback loops and individual cells yield different dynamics in response to different inputs according to their local neighborhood and position (cell-cell adhesion, Delta-Notch signals from cell-cell contacts, diffusible FGF signal, Wnt signal gradient). These signals synchronize clock phases in nearby cells and generate a spatial profile of the clock period, that becomes visible as a phase wave.

### Complete Model

As the submodels successfully reproduced the published results, also the complete model in Morpheus does so:

<figure id="vid-2">
  ![Complete Video](complete_video.mp4)
  <figcaption style="margin-top: -1.75em;">
    <strong>Video 3</strong> shows the complete model simulation in Morpheus with <strong>(left)</strong> cell types color-coded as in <a href="#fig-5">Figure 5</a> (see below), <strong>(middle)</strong> FGF8 concentration field color-coded as given by the color bar next to the data, <strong>(right)</strong> Lfng protein levels color-coded as given by the color bar next to the data.
  </figcaption>
</figure>

The published simulation results were presented in separate [Videos S1 and S2](#vid-3), as shown below:
<figure id="vid-3" style="margin-top: -1em;">
  <div style="display: flex; justify-content: space-between;">
    <div style="flex: 1; padding-right: 5px;">
      ![Published Video S1](video_s1.mp4)
    </div>
    <div style="flex: 1; padding-left: 5px;">
      ![Published Video S2](video_s2.mp4)
    </div>
  </div>
  <figcaption style="margin-top: -1.75em;">
    <strong>Videos S1 and S2</strong> showing results for cell types (<strong>left</strong>, <a href="https://doi.org/10.1371/journal.pcbi.1002155.s016" target="_blank" rel="noopener noreferrer">Video S1</a>) and the extracellular FGF8 field (<strong>right</strong>, <a href="https://doi.org/10.1371/journal.pcbi.1002155.s017" target="_blank" rel="noopener noreferrer">Video S2</a>) in the published reference simulation. (<a href="https://creativecommons.org/licenses/by/4.0/" target="_blank" rel="noopener noreferrer">CC BY 4.0</a>: <a href="#reference"><strong>Hester <em>et al.</em></strong></a>)
  </figcaption>
</figure>

These simulation results can be compared for individual time points as shown below.

<div id="fig-5">
![Snapshot of complete model simulation in publication at 720 min](fig_5.png "<strong>Figure 5.</strong> This is [Figure 5E-G](https://doi.org/10.1371/journal.pcbi.1002155.g005) of the cited publication and shows (E) cell types, (F) FGF8 and (G) Lfng for the complete model simulation in CompuCell3D at 720 min. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Hester _et al._**](#reference))")
</div>

![Snapshot of complete model simulation in Morpheus at 720 min](fig_5_repl.png "<strong>Figure 6.</strong> Snapshot of complete model simulation in Morpheus at 720 min. These results reproduce [Figure 5E-G](https://doi.org/10.1371/journal.pcbi.1002155.g005) of the cited publication. For legend see videos above.")

### Profile of Oscillation Period

[Hester _et al._](#reference) observed an emergent increase of the segmentation clock period from the posterior end towards anterior. We observed the same behavior both qualitatively and quantitatively in the separate model file `submodel_clock.xml` that omits downstream differentiation processes as in the original model. These Morpheus results (panels C,D) match the original (panels A,B) as compared in the following figure:

![Clock period profiles match between the original and Morpheus model](period_comparison.png "<strong>Figure 7.</strong> Comparison of clock period profiles. (A,B) are original results given in [Figure 6](https://doi.org/10.1371/journal.pcbi.1002155.g006) of the cited publication and show (A) period length in individual cells (represented by data points) of a growing PSM as function of _mean Wnt_ concentration across the last period and (B) as function of distance from the posterior tip at the end of the period. (C,D) are reproduced results from Morpheus at 975 minutes simulation time. (C) Period length in individual cells (represented by data points) of a growing PSM as function of _lower Wnt_ concentration at the _end_ of the period and (B) as function of distance from the posterior tip at the end of the period. Clock periods were measured between successive peaks and troughs, respectively. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Hester _et al._**](#reference))")

We reproduced the figure in Morpheus (even measuring the temporal period and constructing the entire plot on the fly in the running model) by measuring in each PSM cell the period length both between subsequent maxima and minima of the Lfng time course, respectively. The slight data shift towards lower Wnt3a in panel (C) compared to (A) resulted from plotting against the _lower_ Wnt3a values at the _end_ of each clock period (in C) instead of the _mean_ Wnt concentration (in A).

Note that daughters of source cells here inherited the clock phase of the mother cell and thereby started in sync. The incorporated Delta-Notch pathway may also dynamically synchronize initially random clock phases, which could be an interesting follow-up question. The supplemental model code as documented in the [appendix of S. Hester's dissertation](https://www.proquest.com/dissertations-theses/multi-scale-cell-based-computational-models/docview/1015164701/se-2) included the possibility to randomize the clock phase upon cell division ([see page 129 and page 149](https://www.proquest.com/dissertations-theses/multi-scale-cell-based-computational-models/docview/1015164701/se-2)). In the Morpheus model, such phase variability could also be introduced by running the `System`s with Clock ODEs individually faster or slower (using the `Scaling` attribute) in daughter cells for some time interval.

In addition to the matching videos above, this reproduction of the period profiles is very sensitive to details in all coupled submodels and confirms the published results in an independent simulator.
