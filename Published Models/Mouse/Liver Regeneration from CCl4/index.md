---
MorpheusModelID: M9147

title: "Liver Regeneration from CCl4"
#linktitle: "Liver Regeneration from CCl4"

authors: [S. Höhme, M. Brulport, A. Bauer, E. Bedawy, W. Schormann, M. Hermes, V. Puppe, R. Gebhardt, S. Zellmer, M. Schwarz, E. Bockamp, T. Timmel, J. G. Hengstler, D. Drasdo]
contributors: [D. Jahn, L. Brusch]
submitters: [D. Jahn, L. Brusch]
curators: [D. Jahn. L. Brusch]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "https://doi.org/10.1073/pnas.0909374107"
  title: "Prediction and validation of cell alignment along microvessels as order principle to restore tissue architecture in liver regeneration"
  journal: "PNAS"
  volume: 107
  issue: 23
  page: "10371--10376"
  year: 2010
  original_model: false
  preprint: false

tags:
- 2D
- Adhesion
- Anisotropic Adhesion
- Apical Adhesion
- Apical Polarity
- Apical Polarization
- Boundary Condition
- BrdU
- Carbon Tetrachloride
- CCl4
- Cell Alignment
- Cell Death
- Cell Division
- Cell Layer
- Cell Migration
- Cellular Potts Model
- Center-Based Model
- Central Vein
- CV
- Chemoattractant
- Chemotaxis
- Concentration Profile
- CPM
- Cross Section
- Domain
- Domain Expression
- Domain Image
- Expression
- Hepatic Lobule
- Hepatocyte
- Hexagonal Domain
- Hexagonal Lattice
- HomophilicAdhesion
- Injury
- Intoxication
- Layer
- Lesion
- Liver
- Liver Damage
- Liver Injury
- Liver Lobule
- Liver Recovery
- Liver Regeneration
- Lobule
- Lobule Microarchitecture
- Mammal
- Mammalian Liver
- MembraneProperty
- Microarchitecture
- Mouse
- Multicellular Model
- Necrosis
- Necrotic Damage
- Necrotic Lesion
- No-Flux
- ODE
- Ordinary Differential Equation
- Oriented Cell Division
- Partial Differential Equation
- PDE
- Pericentral Necrotic Lesion
- Portal Vein
- Proliferation
- PV
- Regeneration
- Regeneration Process
- Spatial Model
- Sinusoid
- Tetrachloromethane
- Tissue Architecture

#categories:
#- DOI:10.1073/pnas.0909374107
#- DOI:10.1007/s11538-017-0375-1
---
> Hepatocyte–sinusoid alignment (HSA) supports the coordination of hepatocytes during liver regeneration after damage by carbon tetrachloride.

## Introduction

The regenerative capacity of the mammalian liver did already inspire the fate of Prometheus in Greek Myth but it still poses many questions today that call for spatio-temporal multicellular modeling. In the experimental paradigm of CCl<sub>4</sub> intoxication of mice, hepatocytes around the central vein (located in the center of an idealized hexagonal liver lobule) are dying and are subsequently replaced by new hepatocytes, largely through activation of proliferation in the remaining hepatocytes, as depicted in the light microscopy images in [Fig. 1](#fig-1) below.

Alignment of daughter hepatocytes along the orientation of the closest sinusoid, a process which is named hepatocyte-sinusoid alignment (HSA), was proposed by [Höhme _et al._](#reference) as necessary mechanism to restore the layered tissue microarchitecture. Alternative hypotheses could not explain the experimental data as well.

Originally, [Höhme _et al._](#reference) employed a center-based model. Here, using the same processes and parameter values wherever possible, the published rersults are reproduced quantitatively by a cellular Potts model (CPM).

<div id="fig-1">
![](hoehme-2010_fig-2a-d_mouse-liver-lobules-light-microscopy.jpg "<strong>Figure 1. Mouse liver lobules visualized under light microscopy.</strong> <strong>(A)</strong> Control, <strong>(B)</strong> $`t = 2\ \mathrm{d}`$, <strong>(C)</strong> $`t = 4\ \mathrm{d}`$, and <strong>(D)</strong> $`t = 8\ \mathrm{d}`$ after administration of CCl<sub>4</sub>. ([©](https://www.pnas.org/about/rights-permissions) [**Höhme _et al._**](#reference), [Fig. 2A-D](https://www.pnas.org/doi/full/10.1073/pnas.0909374107#F2))")
</div>

## Description

The model captures all hepatocytes of a mouse liver lobule during a 16-day regeneration process. The spatial initialization of the lobule including the hexagonal shape, medium, hepatocytes, central vein (CV) and portal veins (PV) was inspired by a two-dimensional <a href="/model/m9496/" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M9496</code>">human liver model</a> originally modeled in MorpheusML by <a href="/model/m9496/#reference" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M9496</code>">Heldring et _al._, 2022</a>. While the original model by [Höhme _et al._](#reference) is three-dimensional, we here focus on a cross section plane and have to scale cell numbers etc. from the original volume to the cross section area. Also, the radially oriented sinusoidal objects can only be incorporated in a three-dimensional model and are omitted in the two-dimensional model. The remaining space is filled by hepatocytes.

Although a regular <span title="//Space/Lattice/Domain/Hexagon">`Hexagon`</span> with <span title="//Space/Lattice/BoundaryConditions/Condition[@boundary='x' or @boundary='y']/@type = 'noflux'">`noflux`</span> boundary conditions was used in the present MorpheusML model, arbitrary lobule geometries as described by [Höhme _et al._](#reference) and shown e.g. in [SI Movie S12](https://www.pnas.org/doi/full/10.1073/pnas.0909374107#supplementary-materials) may also be easily defined in MorpheusML by using a custom domain <span title="//Space/Lattice/Domain/Expression">`Expression`</span> or domain <span title="//Space/Lattice/Domain/Image">`Image`</span>.

Parameters from [SI Appendix Tab. 6](https://www.pnas.org/doi/10.1073/pnas.0909374107#supplementary-materials) by [Höhme _et al._](#reference) were adjusted accordingly:

| Parameter | Symbol (Lit.) | Value (Lit.) | Location in MorpheusML |
|---|:---:|:---:|:---:|
| Lobule radius | $`R`$ | $`284.3\ \mathrm{\mu m}`$ | <span title="//Space/Lattice/Domain/Hexagon/@diameter">`Hexagon`</span> |
| Radius of necrotic lesion before regeneration | $`R_\mathrm{nec}`$ | $`149\ \mathrm{\mu m}`$ | <span title="//Global/Constant[@symbol='hepatocyte.lesion.radius']/@value">`hepatocyte.lesion.radius` |
| Central vein radius | – | $`41.2\ \mathrm{\mu m}`$ | <span title="//CellPopulations/Population[@type='CV']/InitCellObjects/Arrangement/Sphere/@radius">`Sphere`</span> |
| Hepatocyte diameter | $`l_\mathrm{Cell}`$ | $`23.3\ \mathrm{\mu m}`$ | <span title="//CellTypes/CellType[@name = 'hepatocytes']/VolumeConstraint/@target">`VolumeConstraint`</span> |
| Intrinsic cell cycle time | $`\tau`$ | $`24\ \mathrm{h}`$ | <span title="//Global/Constant/@symbol = 'hepatocyte.time_span.cell_cycle'">`hepatocyte.time_span.cell_cycle`</span> |
| Morphogen diffusion coefficient | $`D_\mathrm{M}`$ | $`10^{-6}\ \frac{\mathrm{cm}^2}{\mathrm{s}}`$ | <span title="//Global/Field[@symbol='c']/Diffusion/@rate">`Diffusion`</span>¹ |

<p style="margin-top: -1em; line-height: 0.2;"><small>¹Diffusion rate set smaller than in <a href="#reference">Höhme <em>et al.</em></a> ($`\frac{1}{100}`$ of the lit. value) to significantly reduce simulation runtime.</small></p>

To simulate damage from CCl<sub>4</sub>, a circular necrotic lesion was imposed by a <span title="//CellTypes/CellType[@name='hepatocytes']/CellDeath/Condition">`CellDeath`</span> rule. The units of the variables are chosen as $`[\mathrm{time}] = 1\ \mathrm{d}`$ and $`[\mathrm{space}] = 3\ \mathrm{\mu m}`$. 

<span id="desc-regeneration-mechanisms">Four mechanisms are considered that drive liver regeneration after damage by CCl<sub>4</sub>:</span>

- A time and distance-dependent proliferation response as measured from BrdU incorporation experiments and shown in [Fig. 2](#fig-2) below (<span title="//CellTypes/CellType[@name='hepatocytes']/System/Rule/@symbol-ref = 'hepatocyte.proliferation_p'">`hepatocyte.proliferation_p`</span>),
- cell-cell adhesion such that adjacent polar hepatocytes only form adhesive bonds at their apical sides (<span title="//CPM/Interaction/Contact[type1='hepatocytes' and type2='hepatocytes']/HomophilicAdhesion/@adhesive = 'membrane.apical'">`HomophilicAdhesion`</span>),
- active chemotactic migration of hepatocytes in a field of diffusible factors that emanate from the necrotic lesion (<span title="//CellTypes/CellType[@name='hepatocytes']/Chemotaxis/@field = 'c'">`Chemotaxis`</span>), and
- „hepatocyte-sinusoid alignment” (HSA): Daughter cells from hepatocyte division align along the closest sinusoid (<span title="//CellTypes/CellType[@name='hepatocytes']/CellDivision/@orientation = 'hepatocyte.sinusoid_mean'">`CellDivision`</span>).

<div id="fig-2">
![](hoehme-2010_fig-2e_brdu-positive-cell-distribution.jpg "<strong>Figure 2. Distribution of BrdU-positive cells</strong> in the liver lobule ([©](https://www.pnas.org/about/rights-permissions) [**Höhme _et al._**](#reference), [Fig. 2E](https://www.pnas.org/doi/full/10.1073/pnas.0909374107#F2))")
</div>

The interesting mechanism HSA is implemented in MorpheusML as oriented <span title="//CellTypes/CellType[@name='hepatocytes']/CellDivision">`CellDivision`</span> following the (radial) sinusoidal vector field. This (arbitrary) vector field can alternatively be imported from experimental data. Moreover, hepatocytes possess anisotropic apical adhesion and this is implemented in MorpheusML through a field of adhesion molecule density (a <span title="//CellTypes/CellType[@name='hepatocytes']/MembraneProperty">`MembraneProperty`</span> with highest values at apical domains) along the dynamically reshaping  and moving membranes of each hepatocyte.

## Results

Published results by [Höhme _et al._](#reference) from the center-based model are shown in [Figs.&nbsp;3](fig-3) and [4](#fig-4) and [Video 1](#vid-1). Reproduced results from the cellular Potts model are shown in [Video 1](#vid-1), [Video 2](#vid-2) and [Fig. 5](#fig-5). These simulation results match with the published results.

<figure id="vid-1">
  <div style="display: flex; justify-content: space-between;">
    <div style="flex: 0.9585; padding-right: 5px; margin-top: -2rem; margin-bottom: -2rem;">
      ![](hoehme-2010_sm-9_simulation.mp4)
    </div>
    <div style="flex: 1; padding-left: 5px; margin-top: -2rem; margin-bottom: -2rem;">
      ![](M9147_liver-regeneration_vid-1_simulation.mp4)
    </div>
  </div>
  <figcaption>
    <strong>Video 1. Simulation by <a href="#reference">Höhme <em>et al.</em></a></strong> (<strong>left</strong>, <a href="https://www.pnas.org/about/rights-permissions" target="_blank" rel="noopener noreferrer">©</a>&nbsp;<a href="#reference"><strong>Höhme <em>et al.</em></strong></a>, <a href="https://www.pnas.org/doi/full/10.1073/pnas.0909374107#supplementary-materials" target="_blank" rel="noopener noreferrer">SI Movie S9</a>) and <strong>Morpheus simulation</strong> (<strong>right</strong>) reproducing the published result showing the regeneration process with anisotropic apical adhesion, active directed migration and orientation of the hepatocytes after cell division along the closest sinusoid. Red: CV, blue: PVs, light orange: hepatocytes, orange: pericentral hepatocytes, beige: necrotic hepatocytes, light red: proliferating (left video) or daughter (right video) hepatocytes.
  </figcaption>
</figure>

<div id="fig-3">
![](hoehme-2010_si-fig-2b-regeneration-dynamics.jpg "<strong>Figure 3.</strong> <strong>Liver lobule with maximal necrotic lesion</strong> $2\ \mathrm{d}$ after administration of CCl<sub>4</sub> (red: layer 1 in direct contact with necrosis, green: layer 2 etc.) ([©](https://www.pnas.org/about/rights-permissions) [**Höhme _et al._**](#reference), [SI Fig. 2B](https://www.pnas.org/doi/full/10.1073/pnas.0909374107#supplementary-materials))")
</div>

<figure id="vid-2">
  <div style="margin-bottom: -1.5rem;">
    ![](M9147_liver-regeneration_vid-2_simulation.mp4)
  </div>
  <figcaption>
    <strong>Video 2.</strong> <strong>Morpheus simulation</strong> with <span title="//Global/Constant[@symbol='apical.polarization']/@value = 0">tangential</span> apical polarization and anisotropic <span title="//CPM/Interaction/Contact[@type1='hepatocytes' and @type2='hepatocytes']/HomophilicAdhesion/@adhesive = 'membrane.apical'"><code>HomophilicAdhesion</code></span>, <span title="//CellTypes/CellType[@name='hepatocytes']/Chemotaxis/@field = 'c'"><code>Chemotaxis</code></span> towards the centrilobular dead cell area and oriented cell division during regeneration from CCl<sub>4</sub> induced liver damage. The <strong>top left</strong> panel shows the layer index with the same color code as in <a href="#fig-3">Fig. 3</a>, the <strong>top right</strong> panel indicated the proliferation index with the same color code as in <a href="#fig-2">Fig. 2</a>, in the <strong>bottom left</strong> panel the apical membrane domains (in light green) are shown and the <strong>bottom right</strong> panel visualizes the concentration profile of the chemoattractant that emanates from the necrotic lesion.
  </figcaption>
</figure>

To quantitatively compare the results from both model formalisms, the pericentral necrotic lesion area was measured in paired simulations. This area reaches its maximum at $`t = 1\ \mathrm{d}`$ in both models and follows a very similar time course in the published data from the center-based model in [Fig. 4](#fig-4) and the reproduced CPM model in [Fig. 5](#fig-5). 

<div id="fig-4">
![](hoehme-2010_fig-3e_area-of-necrotic-lesion.jpg "<strong>Figure 4. Simulation by [**Höhme _et al._**](#reference)</strong> Area of central necrosis with the experiment (green box plot) and the simulation (green graph, ‘Model 3’) in comparison. ‘Model 3’ combines all regeneration mechanisms discussed [above](#desc-regeneration-mechanisms), whereas submodels ‘Model 2’ (without HSA) and ‘Model 1’ (including growth dynamics, but with unspecific homogeneous isotropic adhesion, and neither chemotaxis nor HSA) by [Höhme _et al._](#reference) are not compared in the here presented MorpheusML implementation. ([©](https://www.pnas.org/about/rights-permissions) [**Höhme _et al._**](#reference), [Fig. 3E](https://www.pnas.org/doi/full/10.1073/pnas.0909374107#F3))")
</div>

<div id="fig-5">
![](M9147_liver-regeneration_fig-4_logger_1_plot_time.regeneration_hepatocyte.lesion.area_04752.png "<strong>Figure 5. Morpheus Simulation.</strong> Time course of the necrotic lesion in Morpheus' CPM simulation (purple curve) compared to the experimental data from [Höhme _et al._](#reference) (green bars)")
</div>
