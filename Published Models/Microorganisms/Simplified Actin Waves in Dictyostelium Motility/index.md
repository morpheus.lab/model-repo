---
MorpheusModelID: M2071

title: "Simplified Actin Waves in Eukaryotic Cell Motility"
# linktitle: Simplified Actin Waves in Eukaryotic Cell Motility
subtitle: Spatial relaxation oscillator produces spatial dynamics and spiral waves, simplification of a previous model for actin waves

authors: [J. Algorta, A. Fele-Paranj, J. M. Hughes, L. Edelstein-Keshet]
contributors: [J. M. Hughes, L. Edelstein-Keshet]
submitters: [L. Edelstein-Keshet]
curators: [D. Jahn]

# Under review
hidden: true
private: true

# Reference details
publication:
#  doi: ""
  title: "Modeling and Simulating Single and Collective Cell Motility"
  journal: "Cold Spring Harbour Perspectives in Biology"
#  volume: 
#  issue: 
#  page: 
  year: 2024
  original_model: true
  preprint: false

tags:
- 2D
- Actin
- Cell Motility
- Deterministic Model
- Dictyostelium discoideum
- F-actin
- GTPase
- Partial Differential Equation
- PDE
- Polarity
- Signaling
- Spatial Model
- Spiral Wave
- Travelling Wave
- Wave Pinning

#categories:
#- DOI:
---
[hughes2024]: https://doi.org/10.48550/arXiv.2410.12213

> Spatial relaxation oscillator produces spatial dynamics and spiral waves, simplification of a previous model for actin waves

## Introduction

Motivated by alternative cell motility modes in eukaryotic cells like _Dictyostelium discoideum_, we consider a simplified version of an intracellular model for an F-actin negative feedback to an (active/inactive) GTPase system. The equations were analysed by full PDE bifurcation analysis in [Hughes et al., 2024][hughes2024].

## Description

This model is related to a <a href="/model/m2013" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M2013</code>">previous model</a> on actin waves.

Equations: for the active ($`u`$), inactive ($`v`$) GTPase, respectively, and F-actin ($`F`$).

$$\begin{align}
\frac{\partial u}{\partial t} &= (b+\gamma u^2)v - (1+sF+u^2)u + D \Delta u \\\\
\frac{\partial v}{\partial t} &= -(b+\gamma u^2)v + (1+sF+u^2)u + \Delta v \\\\
\frac{\partial F}{\partial t} &= \epsilon (p_0+p_1 u - F) + D_F \Delta F 
\end{align}$$

The domain is periodic, of (side) length $`L`$, in 1D and 2D. 

The parameters and initial conditions are given in the following [table](#tab-1).

<figure id="tab-1">
<div class="d-flex justify-content-center">
<div>

| | | Polar 1D | Traveling Waves 1D | Spiral Waves |
|:---|---|---|---|---|
| **Params.** | $b$ | $0.067$ | $0.067$ | $1.5$ |
| | $\gamma$ | $3.55$ | $3$ | $30$ |
| | $s$ | $0.41$ | $1$ | $10$ |
| | $\epsilon$ | $0.6$ | $0.6$ | $0.6$ |
| | $p_0$ | $0.8$ | $0.8$ | $0.8$ |
| | $p_1$ | $3.8$ | $3.8$ | $3.8$ |
| | $D$ | $0.1$ | $0.1$ | $0.1$ |
| | $D_F$ | $0.001$ | $0.001$ | $0.001$ |
| | $M$ | $2$ | $4.5$ | $3$ |
| | $L$ | $2\pi$ | $pi$ | $10$ |
| **Initial <br /> conditions** | | $\begin{align}\scriptsize u(x, 0) \ &\scriptsize= 1 - 0.5 \cos(x)\\\\ \scriptsize v(x, 0) \ &\scriptsize= 1 - 0.1 \cos(x)\\\\ \scriptsize F(x, 0) \ &\scriptsize= 4.5 - 0.82 \cos(x)\end{align}$ | $\begin{align}\scriptsize u(x, 0) \ &\scriptsize= 1 - 0.5 \cos(x) - 0.6 \sin(x)\\\\ \scriptsize v(x, 0) \ &\scriptsize= 1 - 0.1 \sin(x)\\\\ \scriptsize F(x, 0) \ &\scriptsize= 4.5 - 0.82 \cos(x)\end{align}$ | $\begin{align}\scriptsize u(x, 0) \ &\scriptsize= 0.5\\\\ \scriptsize v(x, 0) \ &\scriptsize= M - 0.5 \scriptsize= 2.5\\\\ \scriptsize F(x, 0) \ &\scriptsize= 4.5 + \chi\end{align}$ |
| **Model <br /> file** | | <span style="white-space: normal; word-wrap: break-word;"><code>ActinWavesPDE<br />1DPolar_main.xml</code><a href="{{< morpheus_link "media/model/m2071/ActinWavesPDE1DPolar_main.xml" "url_only" />}}" title="Open in Morpheus with a click" data-bs-toggle="tooltip"><i class="fas fa-magic pl-1 pr-1"></i></a><a href="{{< model_download "media/model/m2071/ActinWavesPDE1DPolar_main.xml" "url_only" />}}" title="Download XML file" data-bs-toggle="tooltip" download="ActinWavesPDE1DPolar_main.xml"><i class="fas fa-file-download"></i></a></span> | <span style="white-space: normal; word-wrap: break-word;"><code>ActinWavesPDE<br />1DTW.xml</code><a href="{{< morpheus_link "media/model/m2071/ActinWavesPDE1DTW.xml" "url_only" />}}" title="Open in Morpheus with a click" data-bs-toggle="tooltip"><i class="fas fa-magic pl-1 pr-1"></i></a><a href="{{< model_download "media/model/m2071/ActinWavesPDE1DTW.xml" "url_only" />}}" title="Download XML file" data-bs-toggle="tooltip" download="ActinWavesPDE1DTW.xml"><i class="fas fa-file-download"></i></a></span> | <span style="white-space: normal; word-wrap: break-word;"><code>ActinWavesPDE<br />2DSpiral.xml</code><a href="{{< morpheus_link "media/model/m2071/ActinWavesPDE2DSpiral.xml" "url_only" />}}" title="Open in Morpheus with a click" data-bs-toggle="tooltip"><i class="fas fa-magic pl-1 pr-1"></i></a><a href="{{< model_download "media/model/m2071/ActinWavesPDE2DSpiral.xml" "url_only" />}}" title="Download XML file" data-bs-toggle="tooltip" download="ActinWavesPDE2DSpiral.xml"><i class="fas fa-file-download"></i></a></span> |
</div>
</div>
<figcaption><strong>Table 1:</strong> Parameters and initial conditions. Where, for the spiral waves, $\chi = 0.7(0.5 − \sigma)$ for $\sigma$ a normally distributed random number between $[−0.5, 0.5]$, which is adding noise to the initial condition.</figcaption>
</figure>

## Results

### One-dimensional analysis

In 1D spatial coordinates we obtain either a [polar distribution](#fig-1) or a [traveling wave](#fig-2), as shown for the variable $u$ in the two kymographs below. Time evolves (horizontal axis) and space is on the vertical axis.

<div id="fig-1">
![Kymograph of 1D model simulation shows stationary polarized state](ActinWavePolar.png "<strong>Figure 1:</strong> Kymograph of 1D model simulation, with parameters of first column in above table and periodic boundary conditions, shows a **stationary polarized state**.")
</div>

<div id="fig-2">
![Kymograph of 1D model simulation shows traveling wave](ActinWaveTW.png "<strong>Figure 2:</strong> Kymograph of 1D model simulation, with parameters of second column in above table and periodic boundary conditions, shows a **traveling wave** with one active domain.")
</div>

### Two-dimensional analysis

In 2D spatial coordinates, the system can also form dynamic spiral waves as shown in the [snapshot](#fig-3) and [movie](#vid-1) below. 

<div id="fig-3">
![Snapshot of 2D model simulation shows spiral waves](ActinWavesPDE2DSpiral.png "<strong>Figure 3:</strong> Snapshot of 2D model simulation, with parameters of third column in above table and periodic boundary conditions, shows **spiral waves**.")
</div>

<figure id="vid-1">
  <!--div style="margin-bottom: -1.5rem;"-->
  <div>
    ![](Actin_WavesHughesSpiral2D.mp4)
  </div>
  <!--figcaption>
    <strong>Video 1.</strong> ...
  </figcaption-->
</figure>