---
MorpheusModelID: M2072

title: "Eukaryotic Cell Motility Based on Actin Waves on the Cell Edge"
# linktitle: Actin Waves on Eukaryotic Cell Edge
subtitle: Cell motility simulation with a reaction-diffusion model for actin waves on the cell perimeter

authors: [J. Algorta, A. Fele-Paranj, J. M. Hughes, L. Edelstein-Keshet]
contributors: [J. Algorta, L. Edelstein-Keshet]
submitters: [J. Algorta, L. Edelstein-Keshet]
curators: [D. Jahn]

# Under review
hidden: true
private: true

# Reference details
publication:
#  doi: ""
  title: "Modeling and Simulating Single and Collective Cell Motility"
  journal: "Cold Spring Harbour Perspectives in Biology"
#  volume: 
#  issue: 
#  page: 
  year: 2024
  original_model: true
  preprint: false

tags:
- 2D
- Actin
- Actin Wave
- Actin Wave Dynamics
- Cell Edge
- Cell Motility
- Cell Shape
- Cellular Potts Model
- CPM
- Deterministic Model
- Dictyostelium discoideum
- Diffusion
- Eukaryotic Cell
- F-actin
- Filamentous Actin
- GTPase
- Lobed Cell
- Lobed-cell Motility
- Membrane
- MembraneProperty
- Motility
- Multi-Lobed Cell
- Negative Feedback
- Partial Differential Equation
- PDE
- Polar Cell
- Polarity
- Polarization
- Protrusion
- Rac
- Reaction-Diffusion System
- Single Cell
- Spatial Model
- StarConvex
- Three-Lobe Cell
- Turning Cell
- Two-Lobe Cell
- Wave Dynamics

#categories:
#- DOI:
---
[hughes2024]: https://doi.org/10.48550/arXiv.2410.12213
[holmes2012]: https://doi.org/10.1088/1478-3975/9/4/046005
[mata2013]: https://doi.org/10.1016/j.jtbi.2013.06.020
[liu2021]: https://doi.org/10.1007/s00285-021-01550-0

> Cell motility simulation with a reaction-diffusion model for actin waves on the cell perimeter

## Introduction

A partial differential equation model for ‘actin waves’ by [Holmes et al., 2012][holmes2012] was simplified and investigated by PDE bifurcation analysis in [Hughes et al., 2024][hughes2024].


This model is related to our <a href="/model/m2071" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M2071</code>">model of actin waves in 1D</a> and our previous work [Mata et al., 2013][mata2013] and [Liu et al., 2021][liu2021].

## Description

The model accounts for active ($`u`$) and inactive ($`v`$) GTPase (such as Rac) and its nucleation of filamentous actin ($`F`$). We assumed that Rac promotes F-actin assembly, and that F-actin contributes negative feedback (inactivation of Rac). 

The model equations are:

$$\begin{align}
\frac{\partial u}{\partial t} &= (b+\gamma u^2)v - (1+sF+u^2)u + D \Delta u \\\\
\frac{\partial v}{\partial t} &= -(b+\gamma u^2)v + (1+sF+u^2)u + \Delta v \\\\
\frac{\partial F}{\partial t} &= \epsilon (p_0+p_1 u - F) + D_F \Delta F 
\end{align}$$

The dynamics of these equations in a 1D periodic domain, and XML files for simulating the model are given on <a href="/model/m2071" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M2071</code>">model page M2071</a>.

Here we investigate the effect of ‘actin wave’ dynamics on the protrusion and motility of a eukaryotic cell. 

We assumed that the reaction-diffusion model operates on the 1D periodic cell-edge domain of a CPM cell. Using the Morpheus plugin <span title="//CellTypes/CellType[@name='cells']/System[@name='WavePinning with Inhibitor']/StarConvex">`StarConvex`</span> membrane based on the level of F-actin ($`F`$), we simulated the PDEs as a <span title="//CellTypes/CellType[@name='cells']/MembraneProperty[@symbol='u' or @symbol='v' or @symbol='F']">`MembraneProperty`</span> system for a single cell, with parameter values given in the [table](#tab-1) below.

<figure id="tab-1">
<div class="d-flex justify-content-center">
<div>

| | | Polar Cell | Turning Cell  |
|:---|---|:-:|:-:|
| **Parameters** | $b$ | $0.067$ | $0.00067$ |
| | $\gamma$ | $3.55$ | $3$ |
| | $s$ | $0.41$ | $1$ |
| | $\epsilon$ | $0.6$ | $0.6$ |
| | $p_0$ | $0.8$ | $0.8$ |
| | $p_1$ | $3.8$ | $3.8$ |
| | $D$ | $0.1$ | $0.1$ |
| | $D_F$ | $0.001$ | $0.001$ |
| | <span title="//Global/Constant/@lengthscale">`lengthscale`</span> | $\frac{1}{5}$ | $\frac{1}{100}$ |
| **Model file** | | <span style="white-space: normal; word-wrap: break-word;"><code>Polar_Cell_<br />Motility_main.xml</code><a href="{{< morpheus_link "media/model/m2072/Polar_Cell_Motility_main.xml" "url_only" />}}" title="Open in Morpheus with a click" data-bs-toggle="tooltip"><i class="fas fa-magic pl-1 pr-1"></i></a><a href="{{< model_download "media/model/m2072/Polar_Cell_Motility_main.xml" "url_only" />}}" title="Download XML file" data-bs-toggle="tooltip" download="Polar_Cell_Motility_main.xml"><i class="fas fa-file-download"></i></a></span> | <span style="white-space: normal; word-wrap: break-word;"><code>Turning_Cell_<br />Motility.xml</code><a href="{{< morpheus_link "media/model/m2072/Turning_Cell_Motility.xml" "url_only" />}}" title="Open in Morpheus with a click" data-bs-toggle="tooltip"><i class="fas fa-magic pl-1 pr-1"></i></a><a href="{{< model_download "media/model/m2072/Turning_Cell_Motility.xml" "url_only" />}}" title="Download XML file" data-bs-toggle="tooltip" download="Turning_Cell_Motility.xml"><i class="fas fa-file-download"></i></a></span> |
</div>
</div>
<figcaption><strong>Table 1:</strong> Parameter values for the Polar Cell and Turning Cell configurations. For the two-lobe&nbsp;({{< model_quick_access "media/model/m2072/Two_Lobe_Cell.xml" >}}) and three-lobe&nbsp;({{< model_quick_access "media/model/m2072/Three_Lobe_Cell.xml" >}}) configurations, the <span title="//Global/Constant/@lengthscale"><code>lengthscale</code></span> parameter is set as described below, and all other parameters are as for the Turning Cell configuration.</figcaption>
</figure>

To observe distinct behaviors, we picked cells of ‘different sizes’: we used similar kinetic parameters, but changed the <span title="//Global/Constant/@lengthscale">`lengthscale`</span>.

The <span title="//Global/Constant/@lengthscale">`lengthscale`</span> was obtained by adjusting the diffusion coefficients: in the Polar Cell, this coefficient was set to $`\frac{1}{5}`$; in the Turning Cell, to $`\frac{1}{100}`$; in the two-lobe configuration, to $`\frac{1}{50}`$ (twice the Turning Cell coefficient); and in the three-lobe configuration, to $`\frac{1}{33}`$ (three times the Turning Cell coefficient).

## Results

Cell shape and motility vary depending on the type of PDE solution, determined by the parameter values and domain size, $`L`$. Four qualitatively different motility states are compared in <a href="#fig-1">Figure 1 (a)-(d)</a> and the associated <a href="#vid-1">Videos 1-4</a> below. For each case, the corresponding model file is linked in the video caption.

<div id="fig-1">
![](M2072_eukaryotic-cell-motility_fig-1_cell-shapes.jpg "<strong>Figure 1:</strong> **(a)** Polar cell, **(b)** turning cell, **(c)** two-lobe cell, **(d)** three-lobe cell. The latter two are less motile than the first two.")
</div>

<figure id="vid-1">
  <div style="margin-bottom: -1.5rem;">
  <div>
    ![](Polar_Cell_Motility_movie.mp4)
  </div>
  <figcaption>
    <strong>Video 1.</strong> A single stable polarization along the cell edge yields straight cell motility, corresponding to <a href="#fig-1">Figure&nbsp;1(a)</a> and {{< model_quick_access "media/model/m2072/Polar_Cell_Motility_main.xml" >}}
  </figcaption>
</figure>

<figure id="vid-2">
  <div style="margin-bottom: -1.5rem;">
  <div>
    ![](Turning_Cell_Motility_movie.mp4)
  </div>
  <figcaption>
    <strong>Video 2.</strong> A single traveling polarization along the cell edge yields cell turning, corresponding to <a href="#fig-1">Figure&nbsp;1(b)</a> and {{< model_quick_access "media/model/m2072/Turning_Cell_Motility.xml" >}}
  </figcaption>
</figure>

<figure id="vid-3">
  <div style="margin-bottom: -1.5rem;">
  <div>
    ![](Two_Lobe_Cell_movie.mp4)
  </div>
  <figcaption>
    <strong>Video 3.</strong> Two periods of polarization along the cell edge yield a two-lobed cell which may also be turning, corresponding to <a href="#fig-1">Figure&nbsp;1(c)</a> and {{< model_quick_access "media/model/m2072/Two_Lobe_Cell.xml" >}}
  </figcaption>
</figure>

<figure id="vid-4">
  <div style="margin-bottom: -1.5rem;">
  <div>
    ![](Three_Lobe_Cell_movie.mp4)
  </div>
  <figcaption>
    <strong>Video 4.</strong> Three or more periods of polarization along the cell edge yield a three-lobed cell or a four-lobed cell that may also be turning, corresponding to <a href="#fig-1">Figure&nbsp;1(d)</a> and {{< model_quick_access "media/model/m2072/Three_Lobe_Cell.xml" >}}
  </figcaption>
</figure>
